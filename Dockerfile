ARG NEXTCLOUD_RELEASE=latest

FROM nextcloud:${NEXTCLOUD_RELEASE}

ARG S6_RELEASE_VERSION

LABEL org.opencontainers.image.authors="${AUTHORS}"
LABEL org.opencontainers.image.url="${REPO_URL}"
LABEL org.opencontainers.image.licenses="${LICENSE}"

EXPOSE 80

ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_RELEASE_VERSION}/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_RELEASE_VERSION}/s6-overlay-x86_64.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz

ENV NEXTCLOUD_UPDATE=1
ENV S6_CMD_WAIT_FOR_SERVICES_MAXTIME=0

ENTRYPOINT [ "/init" ]

COPY services.d /etc/services.d
